package com.example.kotlintest.viewmodel

import android.app.DownloadManager
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.kotlintest.api.OperationCallback
import com.example.kotlintest.model.ApkInfo
import com.example.kotlintest.repo.ApkInfoDataSource
import java.io.File

class MainActivityViewModel(private val apkInfoRepository: ApkInfoDataSource, private val context: Context): ViewModel() {

    val className = javaClass.name

    private val _apkInfo = MutableLiveData<ApkInfo?>().apply { value = null }
    val apkInfo: LiveData<ApkInfo?> = _apkInfo

    private val _isViewLoading=MutableLiveData<Boolean>()
    val isViewLoading:LiveData<Boolean> = _isViewLoading

    private val _onMessageError=MutableLiveData<Boolean>()
    val onMessageError:LiveData<Boolean> = _onMessageError

    private val _installedPackage = MutableLiveData<PackageInfo?>().apply { value = null }
    val installedPackage:LiveData<PackageInfo?> = _installedPackage

    var downloading: LiveData<Boolean> = apkInfoRepository.downloading
    var downloadError: LiveData<Boolean> = apkInfoRepository.downloadError
    var finishDownload: LiveData<Boolean> =  apkInfoRepository.finishDownload
    var downloadProgress: LiveData<Int> = apkInfoRepository.downloadProgress

    val installing: LiveData<Boolean> = apkInfoRepository.installing
    val installError: LiveData<Boolean> = apkInfoRepository.installError
    val finishInstall: LiveData<Boolean> = apkInfoRepository.finishInstall

    val downloadID: LiveData<Long> = apkInfoRepository.downloadID

    fun loadLatestApkInfo(){
        _isViewLoading.postValue(true)
        _onMessageError.postValue(false)
        apkInfoRepository.getLatestApkInfo(object : OperationCallback<ApkInfo>{
            override fun onError(error: String?) {
                _isViewLoading.postValue(false)
                _onMessageError.postValue(true)
                Log.d(className, error)
            }

            override fun onSuccess(data: ApkInfo?) {
                _isViewLoading.postValue(false)
                if(apkInfo != null) {
                    _apkInfo.postValue(data)
                    Log.d(className, data.toString())
                }
            }
        })
    }


    fun checkIfInstalled() {
        _installedPackage.postValue(null)
        Log.d(className, "Checking if installed")
        val installedPackage = apkInfoRepository.checkIfInstalled()
        if(installedPackage != null) {
            _installedPackage.postValue(installedPackage)
        }
    }

    fun checkDownloadedVersion() {
        apkInfoRepository.checkDownloadedVersion(installedPackage.value)
    }

    fun downloadNewUpdate() {
        apkInfoRepository.downloadNewUpdate(apkInfo.value, installedPackage.value)
    }

}