package com.example.kotlintest.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.kotlintest.repo.ApkInfoDataSource

class ViewModelFactory(private val repository:ApkInfoDataSource, private val context: Context): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainActivityViewModel(repository, context) as T
    }
}