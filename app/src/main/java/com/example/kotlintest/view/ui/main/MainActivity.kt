package com.example.kotlintest

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.kotlintest.di.Injection
import com.example.kotlintest.model.ApkInfo
import com.example.kotlintest.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

const val TAG= "CONSOLE"
class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: MainActivityViewModel


    private val onDownloadComplete: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            //Fetching the download id received with the broadcast
            val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            //Checking if the received broadcast is for our enqueued download by matching download id
            val downloadID = viewModel.downloadID.value
            if (downloadID !=null && downloadID == id) {
                Toast.makeText(this@MainActivity, "Download Completed", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewModel()
        registerReceiver(onDownloadComplete, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
    }

    private fun setupViewModel() {
        viewModel = ViewModelProvider(this, Injection.provideViewModelFactory(this)).get(MainActivityViewModel::class.java)
        viewModel.apkInfo.observe(this, renderApkInfo)
        viewModel.onMessageError.observe(this,onMessageErrorObserver)
        viewModel.isViewLoading.observe(this,isViewLoadingObserver)
        viewModel.installedPackage.observe(this,onCheckInstalledPackage)
        viewModel.downloading.observe(this, onCheckDownloading)
        viewModel.downloadProgress.observe(this, onCheckDownloadProgressBar)
        viewModel.downloadError.observe(this, onDownloadError)
        viewModel.finishDownload.observe(this, onCheckFinishDownload)
        viewModel.finishInstall.observe(this, onCheckFinishInstall)
        viewModel.installError.observe(this, onInstallError)
    }

    private val renderApkInfo = Observer<ApkInfo?> {
        if(it != null) {
            Log.d(TAG, it.toString())

            val installedPackage = viewModel.installedPackage.value
            if (installedPackage != null) {
                if(installedPackage.versionCode>= it.versionCode) {
                    textViewUpdateText.text = "You're on the latest version"
                } else {
                    textViewUpdateText.text = "New Update"
                    viewModel.downloadNewUpdate()
                }
            }
             else {
                buttonInstall.visibility = View.VISIBLE
                textViewUpdateText.text = "No version detected"
            }
        }
    }

    private val isViewLoadingObserver= Observer<Boolean> {
        Log.d(TAG, "isViewLoading $it")
        val visibility=if(it) View.VISIBLE else View.GONE
        progressCircularBar.visibility= visibility
    }

    private val onMessageErrorObserver= Observer<Boolean> {
        Log.d(TAG, "onMessageError $it")
        if(it) {
            textViewError.text= "Something went wrong"
            buttonRetry.setOnClickListener(View.OnClickListener { viewModel.loadLatestApkInfo() })
            buttonRetry.visibility = View.VISIBLE
            buttonInstall.visibility = View.GONE
        }
    }

    private val onCheckInstalledPackage = Observer<PackageInfo?> {
        if(it != null) {
            textViewInstalledVersion.text = it.versionName
            buttonInstall.visibility = View.GONE
        } else {
            textViewInstalledVersion.text = "Not installed"
            buttonInstall.visibility = View.VISIBLE
            buttonInstall.setOnClickListener(View.OnClickListener { viewModel.downloadNewUpdate() })
        }
    }

    private val onCheckDownloading = Observer<Boolean> {
        if(it) {
            textViewError.text = ""
            buttonRetry.visibility = View.GONE
            downloadProgressBar.visibility = View.VISIBLE
            textViewProgress.visibility = View.VISIBLE
            buttonInstall.isEnabled = false
        } else {
            downloadProgressBar.visibility = View.GONE
            textViewProgress.visibility = View.GONE
            buttonInstall.isEnabled = true
        }
    }

    private val onCheckDownloadProgressBar = Observer<Int> {
        if(viewModel.downloading != null && viewModel.downloading.value!!) {
            downloadProgressBar.progress = it
            textViewProgress.text = it.toString()
        }
    }

    private val onDownloadError = Observer<Boolean> {
        if(it) {
            textViewError.text = "Download Error"
            buttonRetry.setOnClickListener(View.OnClickListener { viewModel.downloadNewUpdate() })
            buttonRetry.visibility = View.VISIBLE
            downloadProgressBar.visibility = View.GONE
            textViewProgress.visibility = View.GONE
        }
    }

    private val onCheckFinishDownload = Observer<Boolean> {
        if(it) {
            downloadProgressBar.visibility = View.GONE
            textViewProgress.visibility = View.GONE
        }
    }

    private val onCheckFinishInstall = Observer<Boolean> {
        if(it) {
            viewModel.checkIfInstalled()
            textViewUpdateText.text = "You're on the latest version"
        }
        Log.d("check", it.toString())
    }

    private val onInstallError = Observer<Boolean> {
        if(it) {
            textViewError.text = "Install Error"
            buttonRetry.setOnClickListener(View.OnClickListener { viewModel.checkDownloadedVersion() })
            buttonRetry.visibility = View.VISIBLE
            downloadProgressBar.visibility = View.GONE
            textViewProgress.visibility = View.GONE
            buttonInstall.visibility = View.GONE
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "LOAD")
        val PLAYER_ID_INTENT = "playerID"
        val sharedPref = getSharedPreferences(PLAYER_ID_INTENT, Context.MODE_PRIVATE)
        val playerID = sharedPref.getString(PLAYER_ID_INTENT,"")
        if (playerID != null) {
            textViewPlayerID.visibility = View.VISIBLE
            textViewPlayerID.text = playerID
            Log.d(TAG, "playerID: $playerID")
        } else {
            textViewPlayerID.visibility = View.GONE
        }
        viewModel.checkIfInstalled()
        viewModel.loadLatestApkInfo()
    }

}