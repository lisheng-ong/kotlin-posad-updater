package com.example.kotlintest.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ApkInfo(
    val versionCode: Long,
    val versionName: String,
    val downloadUrl: String
): Parcelable 