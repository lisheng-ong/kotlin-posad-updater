package com.example.kotlintest.service

import android.app.IntentService
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.kotlintest.api.OperationCallback
import com.example.kotlintest.di.Injection
import com.example.kotlintest.model.ApkInfo

class BackgroundUpdateService(private val name: String): IntentService(name) {

    val className = javaClass.name
    val PLAYER_ID_INTENT = "playerID"
    val UPDATE_INTENT = "update"
    val LOGOUT_INTENT = "logout"
    val REBOOT_INTENT = "reboot"

    constructor(): this("backgroundUpdateService")

    override fun onCreate() {
        super.onCreate()
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val CHANNEL_ID = "default"
            val channel = NotificationChannel(CHANNEL_ID,
                "Posad Updater",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            (getSystemService(NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
                channel
            )

            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Posad Updating")
                .setContentText("").build()
            startForeground(1, notification )
        }
    }

    override fun onHandleIntent(workIntent: Intent?) {
       if(workIntent != null && workIntent.getStringExtra(UPDATE_INTENT) == UPDATE_INTENT) {
            val playerID = workIntent.getStringExtra(PLAYER_ID_INTENT)
            if(playerID!= null) {
                Log.d(className, playerID)
                val sharedPreference = this.getSharedPreferences(PLAYER_ID_INTENT, Context.MODE_PRIVATE)
                sharedPreference.edit().putString(PLAYER_ID_INTENT, playerID).apply()
            }
            val repo = Injection.providerRepository(this)
            val packageInfo = repo.checkIfInstalled()
            repo.getLatestApkInfo(object: OperationCallback<ApkInfo> {
            override fun onError(error: String?) {
               Log.d(className, error)
            }

            override fun onSuccess(data: ApkInfo?) {
               if(data != null && packageInfo != null) {
                   if(data.versionCode > packageInfo.versionCode) {
                       repo.downloadNewUpdate(data, packageInfo)
                       Log.d(className, data.toString())
                   } else {
                       Log.d(className, "Current Version is higher than server")
                   }
               }
            }
            })
            Log.d(className,"getting service")
       }
        if(workIntent != null && workIntent.getStringExtra(LOGOUT_INTENT) == LOGOUT_INTENT) {
            val sharedPreference = this.getSharedPreferences(PLAYER_ID_INTENT, Context.MODE_PRIVATE)
            sharedPreference.edit().clear().apply()
            Log.d(className,"clear app preference")
        }

        if(workIntent != null && workIntent.getStringExtra(REBOOT_INTENT) == REBOOT_INTENT) {
            Log.d(className,"reboot")
            val command = "reboot"
            val process = Runtime.getRuntime().exec(arrayOf("su", "-c", command))
            process.waitFor()
        }
    }
}