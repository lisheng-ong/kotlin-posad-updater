package com.example.kotlintest.api

import com.example.kotlintest.model.ApkInfo
import java.sql.ClientInfoStatus

data class ApkInfoResp(
    val errorCode: String,
    val errorMsg: String,
    var apkInfo: ApkInfo?
) {
    fun isSuccess():Boolean= (errorCode=="000")
}