package com.example.kotlintest.api

interface OperationCallback<T> {
    fun onSuccess(data: T?)
    fun onError(error: String?)
}