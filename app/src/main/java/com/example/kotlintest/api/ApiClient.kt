package com.example.kotlintest.api

import com.example.kotlintest.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

    private var updateService:UpdateService?=null

    fun build():UpdateService?{
        var builder: Retrofit.Builder = Retrofit.Builder()
            .baseUrl(BuildConfig.HOST)
            .addConverterFactory(GsonConverterFactory.create())

        var httpClient: OkHttpClient.Builder = OkHttpClient.Builder()

        var retrofit: Retrofit = builder.client(httpClient.build()).build()
        updateService = retrofit.create(UpdateService::class.java)

        return updateService as UpdateService
    }

}