package com.example.kotlintest.api

import com.example.kotlintest.model.ApkInfo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface UpdateService {
    @GET("/api/apkUpdate")
    fun getApkVersion(@Query("api_key") apiKey: String, @Query("playerId") playerId: String?): Call<ApkInfoResp>
}