package com.example.kotlintest.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.example.kotlintest.repo.ApkInfoDataSource
import com.example.kotlintest.repo.ApkInfoRepository
import com.example.kotlintest.viewmodel.ViewModelFactory

object Injection {

//    private val apkInfoDataSource:ApkInfoDataSource = ApkInfoRepository()

    fun providerRepository(context: Context):ApkInfoDataSource{
        return ApkInfoRepository(context)
    }

    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory{
        return ViewModelFactory(providerRepository(context), context)
    }

}