package com.example.kotlintest.repo

import android.content.pm.PackageInfo
import androidx.lifecycle.LiveData
import com.example.kotlintest.api.OperationCallback
import com.example.kotlintest.model.ApkInfo

interface ApkInfoDataSource {
    fun getLatestApkInfo(callback: OperationCallback<ApkInfo>)
    fun checkIfInstalled(): PackageInfo?
    fun downloadNewUpdate(latestApkInfo: ApkInfo?, installedPackage: PackageInfo?)
    fun checkDownloadedVersion(installedPackage: PackageInfo?)
    fun cancel()

    val downloading: LiveData<Boolean>
    val downloadError: LiveData<Boolean>
    val finishDownload: LiveData<Boolean>
    val downloadProgress: LiveData<Int>

    val installing: LiveData<Boolean>
    val installError: LiveData<Boolean>
    val finishInstall: LiveData<Boolean>

    val downloadID: LiveData<Long>
}