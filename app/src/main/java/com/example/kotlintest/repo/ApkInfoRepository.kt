package com.example.kotlintest.repo

import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.kotlintest.BuildConfig
import com.example.kotlintest.api.ApiClient
import com.example.kotlintest.api.ApkInfoResp
import com.example.kotlintest.api.OperationCallback
import com.example.kotlintest.model.ApkInfo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

const val TAG="CONSOLE"
class ApkInfoRepository(private val context: Context): ApkInfoDataSource {

    val className = javaClass.name
    private var call: Call<ApkInfoResp>?=null
    private val _downloading = MutableLiveData<Boolean>()
    override val downloading: LiveData<Boolean> = _downloading

    private val _downloadError = MutableLiveData<Boolean>()
    override val downloadError: LiveData<Boolean> = _downloadError

    private val _finishDownload = MutableLiveData<Boolean>()
    override var finishDownload: LiveData<Boolean> = _finishDownload

    private val _downloadProgress = MutableLiveData<Int>()
    override val downloadProgress: LiveData<Int> = _downloadProgress

    private val _installing = MutableLiveData<Boolean>()
    override val installing: LiveData<Boolean> = _installing

    private val _installError = MutableLiveData<Boolean>()
    override val installError: LiveData<Boolean> = _installError

    private val _finishInstall = MutableLiveData<Boolean>()
    override val finishInstall: LiveData<Boolean> = _finishInstall

    private val _downloadID = MutableLiveData<Long>()
    override val downloadID: LiveData<Long> = _downloadID

    override fun getLatestApkInfo(callback: OperationCallback<ApkInfo>) {
        val PLAYER_ID_INTENT = "playerID"
        val sharedPref = context.getSharedPreferences(PLAYER_ID_INTENT, Context.MODE_PRIVATE)
        val playerID = sharedPref.getString(PLAYER_ID_INTENT,null)

        call=ApiClient.build()?.getApkVersion("23rusdaljk203ad2bl", playerID)
        call?.enqueue(object : Callback<ApkInfoResp> {
            override fun onFailure(call: Call<ApkInfoResp>, t: Throwable) {
                callback.onError(t.message)
            }

            override fun onResponse(call: Call<ApkInfoResp>, response: Response<ApkInfoResp>) {
                response.body()?.let {
                    if(response.isSuccessful && (it.isSuccess())){
                        val apkResponse = it.apkInfo
                        if(apkResponse != null) {
                            val apkInfo = ApkInfo(apkResponse.versionCode, apkResponse.versionName, apkResponse.downloadUrl)
                            Log.v(TAG, "data ${apkInfo.toString()}")
                            callback.onSuccess(apkInfo)
                        } else {
                            callback.onSuccess(null)
                        }

                    }else{
                        callback.onError(it.errorMsg)
                    }
                }
            }
        })
    }

    override fun cancel() {
        call?.let {
            it.cancel()
        }
    }

    override fun checkIfInstalled(): PackageInfo? {
        val pm: PackageManager = context.packageManager
        val packages = pm.getInstalledPackages(0)
        for (pi in packages) {
            if (pi.packageName.contains(BuildConfig.PLAYER_PACKAGE_ID)) {
                Log.d(className, pi.packageName)
                return pi
            }
        }
        return null
    }

    override fun downloadNewUpdate(latestApkInfo: ApkInfo?, packageInfo: PackageInfo?) {
        val file = File(context.getExternalFilesDir(null), "posad.apk")
        if(file.exists()) {
            file.delete();
            Log.d(className, "Delete old apk")
        }
        _downloading.postValue(true)
        _finishDownload.postValue(false)
        _downloadProgress.postValue(0)

        if(latestApkInfo != null) {
            val url: String = latestApkInfo.downloadUrl
//            val url: String = "https://d1u0bugdbpvg6z.cloudfront.net/fdroid/repo/posad-2.5.apk"
            val fileName = "posad.apk"

            val request =
                DownloadManager.Request(Uri.parse(url))
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE) // Visibility of the download Notification
                    .setDestinationInExternalFilesDir(context, null, fileName)
                    .setTitle(fileName) // Title of the Download Notification
                    .setDescription("Downloading") // Description of the Download Notification
                    .setAllowedOverMetered(true) // Set if download is allowed on Mobile network
                    .setAllowedOverRoaming(true) // Set if download is allowed on roaming network

            val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

            if(checkIfDownloading(downloadManager)) {
                Toast.makeText(context, "Currently Downloading", Toast.LENGTH_SHORT)
                    .show()
            } else {
                val downloadID = downloadManager.enqueue(request)
                _downloadID.postValue(downloadID)
                Log.d(className, "DownloadID: " + downloadID.toString())
                val handler = Handler()
                val r = object: Runnable {
                    override fun run() {
                        if(!_finishDownload.value!!) {
                            val cursor = downloadManager.query(DownloadManager.Query().setFilterById(downloadID))
                            if (cursor.moveToFirst()) {
                                val status =
                                    cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))

                                when (status) {
                                    DownloadManager.STATUS_FAILED -> {
                                        Log.d(className,"FAILED")
                                        _downloadError.postValue(true)
                                    }
                                    DownloadManager.STATUS_PAUSED -> {
                                        Log.d(className, "PAUSE}")
                                        _downloadError.postValue(true)
                                    }
                                    DownloadManager.STATUS_PENDING -> {
                                        Log.d(className, "PENDING}")
                                        _downloading.postValue(false)
                                    }
                                    DownloadManager.STATUS_RUNNING -> {
                                        val total =
                                            cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES))
                                        if (total >= 0) {
                                            val downloaded =
                                                cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR))
                                            val progress = (downloaded * 100L / total).toInt()
                                            _downloadProgress.postValue(progress)
                                            Log.d(className,Integer.toString(progress))
                                        }
                                    }
                                    DownloadManager.STATUS_SUCCESSFUL -> {
                                        Log.d(className,"Download complete")
                                        _finishDownload.postValue(true)
                                        _downloadProgress.postValue(100)
                                        handler.removeCallbacksAndMessages(null)
                                        checkDownloadedVersion(packageInfo)
                                    }
                                }
                            }
                            handler.postDelayed(this, 1000)
                        }

                    }
                }
                handler.postDelayed(r, 1000)
            }
        }
    }

    override fun checkDownloadedVersion(installedPackage: PackageInfo?) {
        val file = File(context.getExternalFilesDir(null), "posad.apk")
        val pm: PackageManager = context.packageManager
        val downloadedInfo = pm.getPackageArchiveInfo(file.path, 0)
        if (downloadedInfo != null) {
            var versionCode: Long = 0
            versionCode = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                Log.d(className, java.lang.Long.toString(downloadedInfo.longVersionCode))
                downloadedInfo.longVersionCode
            } else {
                Log.d(className, Integer.toString(downloadedInfo.versionCode))
                downloadedInfo.versionCode.toLong()
            }
            if (installedPackage == null || versionCode > installedPackage.versionCode) {
                installApk(file.path)
            }
        } else {
            Log.d(className, "Cant find file")
            _installError.postValue(true)
        }
    }

    private fun checkIfDownloading(downloadManager: DownloadManager): Boolean {
        val downloadID = downloadID.value
        if(downloadID != null) {
            val cursor = downloadManager.query(DownloadManager.Query().setFilterById(downloadID))
            if (cursor.moveToFirst()) {
                val status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                when (status) {
                    DownloadManager.STATUS_RUNNING -> return true
                    else -> return false
                }
            } else {
                return false
            }
        } else  {
            return false
        }
    }

    private fun installApk(filename: String) {
        val file = File(filename)
        if (file.exists()) {
            Log.d(className, "Apk exist")
            try {
                _installing.postValue(true)
                _installError.postValue(false)
                _finishInstall.postValue(false)
                val command = "pm install -r $filename"
                val proccess = Runtime.getRuntime().exec(arrayOf("su", "-c", command))
                val terminate = proccess.waitFor()
                if(terminate == 0) {
                    _finishInstall.postValue(true)
                    Toast.makeText(context, "App installed", Toast.LENGTH_SHORT).show()
                    Log.d(className, "Apk installed = 0")
                }
                Log.d(className, "Apk installed")
                val handler = Handler()
                handler.postDelayed({
                    val launchIntent: Intent? = context.packageManager.getLaunchIntentForPackage(BuildConfig.PLAYER_PACKAGE_ID)
                    if(launchIntent != null) {
                        Log.d(className, "Launch Activity")
                        context.startActivity(launchIntent)
                    } else {
                        Log.d(className, "Launch Intent null ")
                    }
                }, 5000)

            } catch (e: Exception) {
                e.printStackTrace()
                _installing.postValue(false)
                _installError.postValue(true)
                _finishInstall.postValue(false)
                Log.d(className, "Apk install error ")
                Log.d(className, e.message)
            }
        } else {
            Log.d(className, "Apk not found")
        }
    }
}